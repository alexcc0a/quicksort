package com.nesterov;

public class Main {

    public static void main(String[] args) {
        int[] arr = {64, 34, 25, 12, 22, 11, 90};
        int n = arr.length;
        quickSort(arr, 0, n - 1);

        // Выводим отсортированный массив.
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i] + " ");
        }
    }

    public static void quickSort(int[] arr, int low, int high) {
        if (low < high) {
            // Выбираем опорный элемент.
            int pivot = arr[high];
            // Разделяем массив на две части.
            int i = low - 1;
            for (int j = low; j < high; j++) {
                if (arr[j] < pivot) {
                    i++;
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            int temp = arr[i + 1];
            arr[i + 1] = arr[high];
            arr[high] = temp;
            // Рекурсивно сортируем левую и правую части массива.
            quickSort(arr, low, i);
            quickSort(arr, i + 2, high);
        }
    }
}
